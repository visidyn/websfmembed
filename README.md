# WebSFMEmbed

Aimed at being iframe-ready for easy embedding into vendor portals. Try out this example:

```html
<html>
    <head>
        <title>Fake storefront</title>
        <meta name='viewport' content='initial-scale=1.0'>
        <meta charset="utf-8">
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
        </style>
    </head>
    <body>
        <iframe id='cloud'
            width='450'
            height='450'
            frameborder='0'
            style='border:0'
            scrolling='no'
            src='http://localhost:3001/?key=value' allowfullscreen>
        </iframe>
    </body>
</html>

```