
function decodeBase64(data) {
    console.time('decodeBase64');

    var buffer = [];
    var z = 0;
    var bits = 0;
    var base64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    for (var i = 0; i < data.length; i++) {
        z += base64.indexOf( data[i] );
        bits += 6;
        if(bits >= 8) {
            bits -= 8;
            buffer.push(z >> bits);
            z = z & (Math.pow(2, bits) - 1);
        }
        z = z << 6;
    }

    console.timeEnd('decodeBase64');
    return buffer;
}

onmessage = function(e) {

    var msg = e.data;
    var buffer = msg.data.buffer;

    if (typeof buffer === 'undefined') {
        buffer = decodeBase64(msg.data);
    }

    console.time('pointsWorker');

    var numPts = msg.height * msg.width;
    var xyz_rgb = new Float32Array( numPts * 6 );

    var ar_x = new Uint8Array(4);
    var ar_y = new Uint8Array(4);
    var ar_z = new Uint8Array(4);
    var ar_c = new Uint8Array(4);

    //// Get points
    for (var i = 0, j = numPts, base = 0; i < numPts; ++i, ++j, base += 32) {
        
        var xyzBase = 3 * i;
        var rgbBase = 3 * j;

        ar_x[0] = buffer[ base + 0  ];
        ar_x[1] = buffer[ base + 1  ];
        ar_x[2] = buffer[ base + 2  ];
        ar_x[3] = buffer[ base + 3  ];

        ar_y[0] = buffer[ base + 4  ];
        ar_y[1] = buffer[ base + 5  ];
        ar_y[2] = buffer[ base + 6  ];
        ar_y[3] = buffer[ base + 7  ];

        ar_z[0] = buffer[ base + 8  ];
        ar_z[1] = buffer[ base + 9  ];
        ar_z[2] = buffer[ base + 10 ];
        ar_z[3] = buffer[ base + 11 ];

        ar_c[0] = buffer[ base + 16 ];
        ar_c[1] = buffer[ base + 17 ];
        ar_c[2] = buffer[ base + 18 ];
        ar_c[3] = buffer[ base + 19 ];

        var dv_x = new DataView(ar_x.buffer);
        var dv_y = new DataView(ar_y.buffer);
        var dv_z = new DataView(ar_z.buffer);

        xyz_rgb[ xyzBase     ] = dv_x.getFloat32(0, 1) *  1000;
        xyz_rgb[ xyzBase + 1 ] = dv_y.getFloat32(0, 1) * -1000;
        xyz_rgb[ xyzBase + 2 ] = dv_z.getFloat32(0, 1) * -1000;
        xyz_rgb[ rgbBase     ] = ar_c[2] / 255;
        xyz_rgb[ rgbBase + 1 ] = ar_c[1] / 255;
        xyz_rgb[ rgbBase + 2 ] = ar_c[0] / 255;

    }

    console.timeEnd('pointsWorker');

    postMessage(xyz_rgb.buffer, [xyz_rgb.buffer]);
}
