
function reallyGetUserMedia(camIdx, videoElem) {

    function useUserMediaShim( video ) {
    
        video.loop = true;
        video.muted = true;
        video.src = '/videos/room.mp4';

    } 

    MediaStreamTrack.getSources(function (sourceInfos) {

        var videoSources = [];

        for (var i = 0; i != sourceInfos.length; ++i) {
            var sourceInfo = sourceInfos[i];
            if (sourceInfo.kind === 'video') {
                videoSources.push(sourceInfo.id);
            }
        }

        navigator.getUserMedia = (navigator.getUserMedia       ||
                                  navigator.webkitGetUserMedia ||
                                  navigator.mozGetUserMedia    ||
                                  navigator.msGetUserMedia);

        if (videoSources.length) {
            if (camIdx < 0) {
                console.log('Error,', camIdx, 'is out of bounds, trying cam 0');
                camIdx = 0;
            }
            else {
                while (camIdx >= videoSources.length) {
                    console.log('Error,', camIdx, 'is out of bounds, trying cam', --camIdx);
                }
            }
        }

        else {
            console.log('Error, no video sources');
            useUserMediaShim( videoElem );           
            return;
        }

        function setUserMedia(i) {

            var constraints = {
                video: {
                    optional: [{
                        sourceId: videoSources[i]
                    }],
                    mandatory: {
                        // minWidth: 1920,
                        // maxWidth: 1920,
                        // maxHeight: 720,
                        // minHeight: 720
                        // minWidth: 645,
                        // maxWidth: 656,
                        // maxHeight: 361,
                        // minHeight: 361
                    }
                },
                audio: false
            };
    
            navigator.getUserMedia(constraints, function(stream) {

                videoElem.src = window.URL.createObjectURL(stream);
                videoElem.play();
                console.log('playing video id:', videoSources[i]);

            }, function(error) {
                
                console.log('setUserMedia error: ' + error);
                useUserMediaShim( videoElem );

            });

        }

        setUserMedia(camIdx);

        return function() {
            if (videoSources.length) {
                camIdx = camIdx ? 0 : 1;
                setUserMedia(camIdx);
            }
            else {
                console.log('Error, no video sources');
                useUserMediaShim( videoElem );
            }
        }

    });

}
