
var video = document.getElementById('video');
var canvasGradient = document.getElementById('canvas-gradient');
var canvasVideoSend = document.getElementById('canvas-video-send');
var canvasWebGL = document.getElementById('canvas-webgl');

var contextVideoSend = canvasVideoSend.getContext('2d');
var contextGradient = canvasGradient.getContext('2d');

var videoDims = { width: 640, height: 480 };

canvasVideoSend.width = videoDims.width;
canvasVideoSend.height = videoDims.height;

var toggleCamFn = reallyGetUserMedia(1, video);

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 45, dims().width / dims().height, 1, 5000 );

var renderer = new THREE.WebGLRenderer({ alpha: true, canvas: canvasWebGL });
var poseManager = new PoseManager( camera );
var pointsManager = new PointsManager( scene, 15, 10 );

var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );

setCanvasSize(dims().width, dims().height);

renderer.setClearColor( 0x000000, 0.7 );

var shareWidth;
var shareHeight;


function dims() {
    return {
        width: window.innerWidth,
        height: window.innerHeight
    }
}

function computeShareCanvasSize() {
    // TODO: Find less hacky way than 18px offset to move
    //       embed canvas directly under iframe example
    var shareEmbed = document.getElementById('share-embed');
    var shareEmbedText = document.getElementById('share-embed-text');
    var shareEmbedCode = document.getElementById('share-embed-code');
    var shareFooter = document.getElementById('share-footer');
    shareWidth = shareEmbedCode.clientWidth;
    shareHeight = shareEmbed.clientHeight -
                  shareEmbedCode.clientHeight -
                  shareEmbedText.clientHeight -
                  shareFooter.clientHeight + 18;
}

function setCanvasSize(width, height) {

    console.log('setting canvas size', width, height);

    if (renderer.getPixelRatio() == 1) {
        renderer.setPixelRatio(window.devicePixelRatio);
    }

    renderer.setSize(width, height);
    camera.aspect = width / height;
    camera.updateProjectionMatrix();

}

window.onresize = e => {

    var width = dims().width;
    var height = dims().height;

    if (shareWidth || shareHeight) {
        // In share state
        computeShareCanvasSize();
        width = shareWidth;
        height = shareHeight;
    }

    setCanvasSize( width, height );

}

var promiseStart = () => {

    var tapToStartDiv = document.getElementById('tap-to-start-text');

    return new Promise((resolve, reject) => {

        function startUpHandler() {

            // if (screenfull.enabled) {
            //     screenfull.request();
            // }

            cleanup();
            tapToStartDiv.setAttribute('hidden', 'true');
            renderer.setClearColor( 0x000000, 0.0 );
            console.log('started');
            
            resolve();
        }

        function startDownHandler(e) {
            console.log('press');

            window.removeEventListener('touchstart', startDownHandler);
            window.removeEventListener('mousedown', startDownHandler);

            window.addEventListener('touchend', startUpHandler);
            window.addEventListener('mouseup', startUpHandler);

        }

        function cleanup() {
            window.removeEventListener('touchend', startUpHandler);
            window.removeEventListener('mouseup', startUpHandler);
        }

        window.addEventListener('touchstart', startDownHandler);
        window.addEventListener('mousedown', startDownHandler);

    });

}

var promiseCapture = () => {

    var publishMs = 33;
    var rosClient = new ROSClient();

    var captureDiv = document.getElementById('capture');
    var dimDiv = document.getElementById('dim');
    var captureButtons = document.getElementById('capture-btns');
    var reviewButtons = document.getElementById('review-btns');
    var resetButton = document.getElementById('reset-btn');
    var doneButton = document.getElementById('done-btn');

    rosClient.onPoints = points => { pointsManager.onPoints( points ) };
    rosClient.onPose = pose => { poseManager.onPose( pose ) };

    return new Promise((resolve, reject) => {

        var publishTimer;

        function captureOnHandler(e) {

            e.preventDefault();
            // Tried dim capture div w/ CSS was having issues w/ z-index
            renderer.setClearColor( 0x000000, 0.7 );
            resetButton.setAttribute('style', 'opacity: 0.5;');
            doneButton.setAttribute('style', 'opacity: 0.5;');

            publishTimer = setInterval(() => {
                contextVideoSend.drawImage(
                    video,
                    0, 0, videoDims.width, videoDims.height,
                    0, 0, videoDims.width, videoDims.height);
                rosClient.publishImage( canvasVideoSend );
            }, publishMs);

        }

        function captureOffHandler(e) {

            renderer.setClearColor( 0x000000, 0 );
            resetButton.setAttribute('style', 'opacity: 1;');
            doneButton.setAttribute('style', 'opacity: 1;');

            if ( publishTimer ) {
                clearInterval( publishTimer );
            }
            else {
                console.log('no publish timer to clear')
            }

        }

        function resetHandler(e) {

            pointsManager.clearScene();
            rosClient.publishReset();

        }

        function doneHandler(e) {

            cleanup();
            resolve();

        }

        function cleanup() {

//            pointsManager.writePLY();

            video.classList.add('slide-left');
            captureButtons.classList.add('slide-left');
            captureButtons.classList.remove('slide-up');

            canvasWebGL.removeEventListener('touchstart', captureOnHandler);
            canvasWebGL.removeEventListener('mousedown', captureOnHandler);

            canvasWebGL.removeEventListener('touchend', captureOffHandler);
            canvasWebGL.removeEventListener('mouseup', captureOffHandler);

            resetButton.removeEventListener('touchstart', resetHandler);
            resetButton.removeEventListener('click', resetHandler);

            doneButton.removeEventListener('touchstart', doneHandler);
            doneButton.removeEventListener('click', doneHandler);
        }
        
        captureButtons.classList.add('slide-up');

        canvasWebGL.addEventListener('touchstart', captureOnHandler);
        canvasWebGL.addEventListener('mousedown', captureOnHandler);

        canvasWebGL.addEventListener('touchend', captureOffHandler);
        canvasWebGL.addEventListener('mouseup', captureOffHandler);

        resetButton.addEventListener('touchstart', resetHandler);
        resetButton.addEventListener('click', resetHandler);

        doneButton.addEventListener('touchstart', doneHandler);
        doneButton.addEventListener('click', doneHandler);

    });

}


var promiseReview = () => {

    var captureDiv = document.getElementById('capture');
    var reviewDiv = document.getElementById('review');
    var reviewButtons = document.getElementById('review-btns');
    var backButton = document.getElementById('back-btn');
    var uploadButton = document.getElementById('upload-btn');

    return new Promise((resolve, reject) => {

        function backHandler(e) {
            console.log('reset');
            cleanup();
            promiseCapture().then(promiseReview);
            return;
        }

        function uploadHandler(e) {
            cleanup();
            resolve();
        }

        function cleanup() {

            captureDiv.classList.remove('slide-left');
            reviewButtons.classList.remove('fade-in-scale');

            backButton.removeEventListener('touchstart', backHandler);
            backButton.removeEventListener('click', backHandler);

            uploadButton.removeEventListener('touchstart', uploadHandler);
            uploadButton.removeEventListener('click', uploadHandler);

        }


        poseManager.setTrackballControls();

        var centroid = pointsManager.computeCentroid();

        // Move points and camera
        pointsManager.offsetPoints( centroid.clone().negate() );
        camera.position.sub( centroid );

        reviewDiv.classList.remove('off');
        reviewButtons.classList.add('fade-in-scale');

        backButton.addEventListener('touchstart', backHandler);
        backButton.addEventListener('click', backHandler);

        uploadButton.addEventListener('touchstart', uploadHandler);
        uploadButton.addEventListener('click', uploadHandler);

    });

}

var promiseUpload = () => {

    var uploadDiv = document.getElementById('upload');
    var dimDiv = document.getElementById('dim');

    return new Promise((resolve, reject) => {

        function cleanup() {
            canvasWebGL.setAttribute('style', 'opacity: 1');
            uploadDiv.classList.add('off');

        }

        dimDiv.classList.add('show');
        canvasWebGL.setAttribute('style', 'opacity: 0.3');
        uploadDiv.classList.remove('off');

        // Simulate uploading pointcloud to server
        var uploadTimer = setTimeout(() => {
            console.log('uploading');
            cleanup();
            resolve();
        }, 1000);

    });

}

var promiseShare = () => {

    return new Promise((resolve, reject) => {

        var shareDiv = document.getElementById('share');
        var shareFooter = document.getElementById('share-footer');
        shareDiv.classList.remove('off');

        computeShareCanvasSize();

        canvasWebGL.style.top = shareDiv.clientHeight - 
                                shareFooter.clientHeight -
                                shareHeight + 'px';

        setCanvasSize( shareWidth, shareHeight );

        resolve();

    });

}

promiseStart()
    .then(promiseCapture)
    .then(promiseReview)
    .then(promiseUpload)
    .then(promiseShare)
    .then(function() {

    console.log('Done');

}).catch(function(error) {

    console.log('error:', error);

});

function render() {

    cube.rotation.x += 0.1;
    cube.rotation.y += 0.1;

    poseManager.update();

    camera.updateProjectionMatrix(); 
    requestAnimationFrame( render );
    renderer.render(scene, camera);

}

render();
