'use strict'

class PointsManager {

    constructor( scene, keepLastNClouds, pointSize ) {

        this.scene = scene;
        this.keepLastNClouds = keepLastNClouds;
        this.pointSize = pointSize;
        this.offset = new THREE.Vector3();
        this.pointsList = [];

    }

    processPoints( pointsRaw ) {

        var xyzRgb = new Float32Array(pointsRaw);
        var numElements = xyzRgb.length / 2;

        var positions = xyzRgb.subarray(0, numElements);
        var colors = xyzRgb.subarray(numElements, numElements * 2);

        var geometry = new THREE.BufferGeometry();
        var material = new THREE.PointsMaterial({
            size: this.pointSize,
            vertexColors: THREE.VertexColors
        });

        geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3) );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3) );
        geometry.computeBoundingBox();

        return new THREE.Points( geometry, material );

    }

    onPoints ( pointsRaw ) {

        var points = this.processPoints( pointsRaw );

        points.position.add( this.offset );

        this.scene.add( points );
        this.pointsList.push( points );

        if ( this.pointsList.length > this.keepLastNClouds ) {
            console.log('Removing oldest pointcloud');
            this.scene.remove( this.pointsList[0] );
            this.pointsList.splice(0, 1);
        }

    }

    offsetPoints( vector3 ) {

        this.pointsList.forEach(function( points ) {
            points.position.add( vector3 );
        });

        this.offset = vector3;

    }

    computeCentroid() {

        var centroid = new THREE.Vector3();
        var pointsCount = 0;

        this.pointsList.forEach(function( points ) {

            var array = points.geometry.getAttribute('position').array;
            for (var i = 0; i < array.length; i += 3) {

                centroid.add( new THREE.Vector3(
                    array[ i + 0 ],
                    array[ i + 1 ],
                    array[ i + 2 ]
                ));

            }
            pointsCount += array.length;

        });

        centroid.divideScalar( pointsCount / 3 );
        return centroid;

    }

    clearScene() {

        var scene = this.scene;

        this.pointsList.forEach(function( points ) {
            scene.remove( points );
        });

    }

    writePLY() {

        var vertexes = [];

        this.pointsList.forEach(function( points ) {

            var geometry = points.geometry;

            var positions = geometry.getAttribute('position').array;
            var colors = geometry.getAttribute('color').array;

            var numVertexes = positions.length / 3;

            for (var i = 0; i < positions.length; i += 3) {

                var x = positions[ i     ] / 1000;
                var y = positions[ i + 1 ] / 1000;
                var z = positions[ i + 2 ] / 1000;
                var r = parseInt(colors[ i     ] * 255);
                var g = parseInt(colors[ i + 1 ] * 255);
                var b = parseInt(colors[ i + 2 ] * 255);

                vertexes.push([x, y, z, r, g, b]);

            }

        });

        var plyStr = 'ply\n' +
                     'format ascii 1.0\n' +
                     'comment Photon generated\n' +
                     'element vertex ' + vertexes.length + '\n' +
                     'property float x\n' +
                     'property float y\n' +
                     'property float z\n' +
                     'property uchar red\n' +
                     'property uchar green\n' +
                     'property uchar blue\n' +
                     'end_header\n';

        vertexes.forEach(function(vertex) {
            plyStr += vertex.join(' ') + '\n';
        });

        var plyBlob = new Blob( [plyStr], {type: 'text/plain;charset=utf-8'} );

        saveAs(plyBlob, 'photonCloud.ply', true);

    }

}
