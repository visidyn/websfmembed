'use strict'

class PoseManager {

    constructor( managed ) {

        this.managed = managed;

        this.controls = null;
        this.lerpPosition = 0.4;
        this.lerpRotation = 0.5;
        
        this.orientationEvent = false;

        this.quatOrientations = {
            device: {
                live: new THREE.Quaternion(),
                slamRef: new THREE.Quaternion()
            },
            slam: new THREE.Quaternion(),
            curr: new THREE.Quaternion()
        };

        this.position = new THREE.Vector3();

        var scope = this;

        window.addEventListener( 'deviceorientation', function ( deviceOrientation ) {

            if (deviceOrientation.alpha == 0 ||
                deviceOrientation.beta  == 0 || 
                deviceOrientation.gamma == 0) {
                console.log('Device emits deviceorientation messages but probably doesn\'t have gyroscope');
            }
            else if (isNaN(deviceOrientation.alpha) ||
                     isNaN(deviceOrientation.beta ) ||
                     isNaN(deviceOrientation.gamma)) {
                console.log('NaN\'s');
            }
            else if (deviceOrientation.alpha &&
                     deviceOrientation.beta  &&
                     deviceOrientation.gamma) {
                
                scope.orientationEvent = true;
                var quatScreenOrientation = new THREE.Quaternion();
                var quatBackToTop = new THREE.Quaternion(-Math.sqrt( 0.5 ), 0, 0, Math.sqrt( 0.5 ));
                var quatDevice = new THREE.Quaternion();

                var alpha  = THREE.Math.degToRad( deviceOrientation.alpha );
                var beta   = THREE.Math.degToRad( deviceOrientation.beta  );
                var gamma  = THREE.Math.degToRad( deviceOrientation.gamma );
                
                // window.orientation = 90 for landscape, 0 for portrait
                var angleScreenOrientation = window.orientation ? THREE.Math.degToRad( window.orientation ) : 0; 
                quatScreenOrientation.setFromAxisAngle( new THREE.Vector3(0, 0, 1), -angleScreenOrientation );

                var euler = new THREE.Euler( beta, alpha, -gamma, 'YXZ' ); // 'ZXY' for w3c but 'YXZ' for three.js
                quatDevice.setFromEuler( euler );                          // orient the device
                quatDevice.multiply( quatBackToTop );                      // camera looks out the back of the device, not the top
                quatDevice.multiply( quatScreenOrientation );              // adjust for screen orientation
                quatDevice = quatDevice.normalize();

                scope.quatOrientations['device']['live'] = quatDevice;

                var quatDelta = scope.quatOrientations['device']['slamRef'].clone().multiply( quatDevice );
                var quatSLAMAdjusted = scope.quatOrientations['slam'].clone().multiply( quatDelta );

                // scope.setRotation( quatSLAMAdjusted );

            }
            else {
                console.log('Device not emitting deviceorientation messages');
            }

        }, false);

    }

    onPose( pose ) {

        this.position = new THREE.Vector3 (
            pose.position.x *  1000,
            pose.position.y * -1000,
            pose.position.z * -1000
        );

        var quatSLAM = new THREE.Quaternion( pose.orientation.x,
                                            -pose.orientation.y,
                                            -pose.orientation.z,
                                             pose.orientation.w);
        quatSLAM = quatSLAM.normalize();

        if (this.orientationEvent) {
            this.quatOrientations['device']['slamRef'] = this.quatOrientations['device']['live'];
            // this.quatOrientations['slam'] = quatSLAM;
            this.quatOrientations['curr'] = quatSLAM;
        }
        else {
            this.setRotation( quatSLAM, 'ZYX') ;
        }

    }

    setPosition( position ) {

        this.managed.position.lerp( position, this.lerpPosition );

    }

    setRotation( quat, order ) {

        this.managed.rotation.setFromQuaternion (
            this.quatOrientations['curr'].slerp( quat, this.lerpRotation ),
            typeof order === 'undefined' ? 'XYZ' : order
        );

    }

    setTrackballControls() {

        this.controls = new THREE.TrackballControls( this.managed );
        this.controls.rotateSpeed = 1.0;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;
        this.controls.noZoom = false;
        this.controls.noPan = false;
        this.controls.staticMoving = true;
        this.controls.dynamicDampingFactor = 0.3;
        this.controls.keys = [ 65, 83, 68 ];

    }

    update() {


        
        if (this.controls) {
            this.controls.update();
        }
        else {
            this.setRotation( this.quatOrientations['curr'] );
            this.setPosition( this.position );
        }

    }

    printQuat( name, quat ) {
        console.log(name + ': [' + quat.x + ', ' + quat.y + ', ' + quat.z + ', ' + quat.w + ']' );
    }

}
