'use strict'

class ROSClient {

    constructor() {

        var ros = new ROSLIB.Ros();
        var worker = new Worker('js/cloud-worker.js');
        var pointSize = 7;
        var scope = this;

        this.topics = {};

        ros.on('connection', function() {
            console.log('Connected to websocket server.');
        });

        ros.on('error', function(error) {
            console.log('Error connecting to websocket server: ', error);
        });

        ros.on('close', function() {
            console.log('Connection to websocket server closed.');
        });

        this.topics['image'] = new ROSLIB.Topic({
            ros: ros,
            name: '/websfm/input/compressed',
            messageType: 'sensor_msgs/CompressedImage'
        });

        this.topics['reset'] = new ROSLIB.Topic({
            ros: ros,
            name: '/websfm/input/reset',
            messageType: 'std_msgs/Empty'
        });

        this.topics['pose'] = new ROSLIB.Topic({
            ros: ros,
            name: '/lsd_slam/pose',
            messageType: 'geometry_msgs/PoseStamped'
        });

        this.topics['points'] = new ROSLIB.Topic({
            ros: ros,
            name: '/websfm/output/points',
            messageType: 'sensor_msgs/PointCloud2'
        });

        this.topics['points'].subscribe(function( msg ) {
            worker.postMessage( msg );
            console.time('workerPost');
        });

        this.topics['pose'].subscribe(function( msg ) {
            scope.onPose( msg.pose );
        });

        worker.onmessage = function( e ) {

            console.timeEnd('workerPost');
            scope.onPoints( e.data );

        }

        ros.connect('wss://' + window.location.hostname + ':9090');

        this.blobMethod = 'toBlob';

        this.numPics = 0;
        this.startTime;

        this.publishReset();

    }
    
    publishImage( canvas ) {

        var scope = this;

        function timer() {
            if (scope.numPics == 0) {
                scope.startTime = performance.now();
            }
            if (scope.numPics == 100) {
                var seconds = ( performance.now() - scope.startTime ) / 1000;
                console.log('Method:', scope.blobMethod, '::', canvas.width, 'x', canvas.height, '::', seconds, 's');
            }
            scope.numPics++;
        }

        if (canvas.toBlob) {

            // console.log('Using toBlob');

            canvas.toBlob(function( blob ) {

                var reader = new FileReader();

                reader.addEventListener('loadend', function() {

                    var imageMessage = new ROSLIB.Message({
                        format: 'jpeg',
                        data: reader.result.replace('data:image/jpeg;base64,', '')
                    });

                    scope.topics['image'].publish( imageMessage );

                });

                reader.readAsDataURL( blob );

            }, 'image/jpeg');

            timer();

        }

        else {

            // console.log('Using toDataURL');

            var imageMessage = new ROSLIB.Message({
                format : 'jpeg',
                data : canvas.toDataURL('image/jpeg').replace('data:image/jpeg;base64,', '')
            });

            this.topics['image'].publish( imageMessage );

            timer();
        }

    
    }

    publishReset() {

        this.topics['reset'].publish();

        console.log('Published reset');

    }


};